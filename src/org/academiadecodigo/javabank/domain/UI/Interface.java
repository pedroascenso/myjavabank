package org.academiadecodigo.javabank.domain.UI;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Interface {

    private Prompt prompt;
    private Bank bank;
    private Customer currentUser;
    private boolean logged;

    public Interface(){
        this.prompt = new Prompt(System.in, System.out);
        this.bank = new Bank(new AccountManager());
        this.logged = false;
    }

    public static void main(String[] args) {
        Interface bla = new Interface();
        bla.startMenu();
    }

    public void startMenu() {
        System.out.println("WELCOME TO JAVABANK");
        String[] options = {"LOGIN", "ADD CUSTOMER","LIST USERS", "QUIT"};
        MenuInputScanner menuScan = new MenuInputScanner(options);
        menuScan.setMessage("PICK YOUR OPTION");
        int optionIndex = prompt.getUserInput(menuScan);

        while (optionIndex < 1 || optionIndex > options.length) {
            menuScan.setError("PICK ONE OF THE AVAILABLE OPTIONS");
            menuScan.error(System.out);
            optionIndex = prompt.getUserInput(menuScan);
        }
        switch (optionIndex){
            case 1:
                login();
                break;
            case 2:
                addCustomer();
                break;
            case 3:
                this.bank.printCustomers();
                startMenu();
                break;
            case 4:
                quit();
                break;
        }
    }

    public void login(){
        if(this.bank.getCustomers().size() == 0){
            System.out.println("YOU NEED TO CREATE A USER FIRST");
            startMenu();
        }
        StringInputScanner usernameScan = new StringInputScanner();
        usernameScan.setMessage("Username: ");
        usernameScan.setError("USER NOT FOUND");
        String username = prompt.getUserInput(usernameScan);

        StringInputScanner passwordScan = new StringInputScanner();
        passwordScan.setMessage("Password: ");
        passwordScan.setError("OR PASSWORD INCORRECT");

        String password = prompt.getUserInput(passwordScan);

        this.currentUser = this.bank.getCustomer(username, password);
        while (!this.bank.getCustomers().contains(currentUser)){
            usernameScan.error(System.out);
            passwordScan.error(System.out);
            username = prompt.getUserInput(usernameScan);
            password = prompt.getUserInput(passwordScan);
            this.currentUser = this.bank.getCustomer(username, password);
        }
        System.out.println("WELCOME " + currentUser.getUsername().toUpperCase());
        this.logged = true;
        menu();
    }

    public void addCustomer(){
        StringInputScanner usernameScan = new StringInputScanner();
        usernameScan.setMessage("Username: ");
        String username = prompt.getUserInput(usernameScan);
        while (username.length() < 4){
            usernameScan.setError("USERNAME MUST CONTAIN MORE THAN 4 CHARACTERS");
            usernameScan.error(System.out);
            username = prompt.getUserInput(usernameScan);
        }
        StringInputScanner passwordScan = new StringInputScanner();
        passwordScan.setMessage("Password: ");
        String password = prompt.getUserInput(passwordScan);
        while (password.length() < 6){
            passwordScan.setError("PASSWORD MUST CONTAIN MORE THAN 6 CHARACTERES");
            passwordScan.error(System.out);
            password = prompt.getUserInput(passwordScan);
        }
        this.bank.addCustomer(new Customer(username, password));
        System.out.println("CUSTOMER ADDED");
        if (this.logged){
            menu();
        } else{
            startMenu();
        }
    }

    public void menu(){
        String[] options = {"VIEW BALANCE", "MAKE DEPOSIT", "MAKE WITHDRAWAL", "OPEN ACCOUNT", "LOGOUT"};
        MenuInputScanner menuScan = new MenuInputScanner(options);
        menuScan.setMessage("PICK YOUR OPTION");
        int optionIndex = prompt.getUserInput(menuScan);
        while (optionIndex < 1 || optionIndex > options.length) {
            menuScan.setError("PICK ONE OF THE AVAILABLE OPTIONS");
            menuScan.error(System.out);
            optionIndex = prompt.getUserInput(menuScan);
        }
        switch (optionIndex){
            case 1:
                getBalance();
                break;
            case 2:
                makeDeposit();
                break;
            case 3:
                makeWithdrawal();
                break;
            case 4:
                openAccount();
                break;
            case 5:
                logout();
                break;
        }
    }

    public void makeWithdrawal(){
        if (this.currentUser.getAccounts().size() == 0){
            System.out.println("YOU DON'T HAVE ANY ACCOUNT OPEN");
            menu();
        }
        if(currentUser.getBalance() <= 0 ){
            System.out.println("YOU NEED TO HAVE MONEY TO BE ABLE TO WITHDRAW");
            menu();
        }
        this.currentUser.listAccounts();
        IntegerInputScanner pickAccount = new IntegerInputScanner();
        pickAccount.setMessage("PICK YOUR ACCOUNT");
        int option = prompt.getUserInput(pickAccount);
        while(! this.currentUser.getAccounts().containsKey(option)){
            pickAccount.setError("ACCOUNT NOT FOUND");
            pickAccount.error(System.out);
            option = prompt.getUserInput(pickAccount);
        }
        IntegerInputScanner pickAmmount = new IntegerInputScanner();
        pickAmmount.setMessage("HOW MANY TO BE WITHDRAWAL");
        int option2 = prompt.getUserInput(pickAmmount);
        while ( option2 < 0 ){
            pickAmmount.setError("YOU NEED TO ENTER A POSITIVE NUMBER");
            pickAmmount.error(System.out);
            option2 = prompt.getUserInput(pickAmmount);
        }
        if (currentUser.getBalance(option) < option2){
            System.out.println("YOU DON'T HAVE THAT AMMOUNT IN THAT ACCOUNT");
            menu();
        }
        this.bank.getAccountManager().withdraw(option, option2);
        System.out.println("WITHDRAW: " + option2 + " IN THE ACCOUNT " + option);
        menu();
    }


    public void makeDeposit(){
        if (this.currentUser.getAccounts().size() == 0){
            System.out.println("YOU DON'T HAVE ANY ACCOUNT OPEN");
            menu();
        }
        this.currentUser.listAccounts();
        IntegerInputScanner pickAccount = new IntegerInputScanner();
        pickAccount.setMessage("PICK YOUR ACCOUNT");
        int option = prompt.getUserInput(pickAccount);
        while(! this.currentUser.getAccounts().containsKey(option)){
            pickAccount.setError("ACCOUNT NOT FOUND");
            pickAccount.error(System.out);
            option = prompt.getUserInput(pickAccount);
        }
        IntegerInputScanner pickAmmount = new IntegerInputScanner();
        pickAmmount.setMessage("HOW MANY TO BE DEPOSITED");
        int option2 = prompt.getUserInput(pickAmmount);
        while ( option2 < 0 ){
            pickAmmount.setError("YOU NEED TO ENTER A POSITIVE NUMBER");
            pickAmmount.error(System.out);
            option2 = prompt.getUserInput(pickAmmount);
        }
        this.bank.getAccountManager().deposit(option, option2);
        System.out.println("DEPOSITED: " + option2 + " IN THE ACCOUNT " + option);
        menu();
    }

    public void openAccount(){
        String[] options = {"CHECKING ACCOUNT", "SAVINGS ACCOUNT"};
        MenuInputScanner menuScan = new MenuInputScanner(options);
        menuScan.setMessage("PICK YOUR OPTION");
        int optionIndex = prompt.getUserInput(menuScan);
        while (optionIndex < 1 || optionIndex > options.length) {
            menuScan.setError("PICK ONE OF THE AVAILABLE OPTIONS");
            menuScan.error(System.out);
            optionIndex = prompt.getUserInput(menuScan);
        }
        switch (optionIndex){
            case 1:
                this.currentUser.openAccount(AccountType.CHECKING);
                System.out.println("CHECKING ACCOUNT CREATED");
                break;
            case 2:
                this.currentUser.openAccount(AccountType.SAVINGS);
                System.out.println("SAVINGS ACCOUNT CREATED");
                break;
        }
        menu();
    }

    public void getBalance(){
        if (this.currentUser.getAccounts().size() == 0){
            System.out.println("YOU DON'T HAVE ANY ACCOUNT OPEN");
            menu();
        }
        String[] options = {"TOTAL BALANCE", "ACCOUNT BALANCE"};
        MenuInputScanner menuScan = new MenuInputScanner(options);
        menuScan.setMessage("PICK YOUR OPTION");
        int optionIndex = prompt.getUserInput(menuScan);
        while (optionIndex < 1 || optionIndex > options.length) {
            menuScan.setError("PICK ONE OF THE AVAILABLE OPTIONS");
            menuScan.error(System.out);
            optionIndex = prompt.getUserInput(menuScan);
        }
        switch (optionIndex){
            case 1:
                System.out.println("YOUR TOTAL BALANCE IS: " + currentUser.getBalance());
                menu();
                break;
            case 2:
                individualBalance();
                break;
        }
    }

    public void individualBalance(){
        this.currentUser.listAccounts();
        IntegerInputScanner pickAccount = new IntegerInputScanner();
        pickAccount.setMessage("PICK YOUR ACCOUNT");
        int option = prompt.getUserInput(pickAccount);
        while(! this.currentUser.getAccounts().containsKey(option)){
            pickAccount.setError("ACCOUNT NOT FOUND");
            pickAccount.error(System.out);
            option = prompt.getUserInput(pickAccount);
        }
        System.out.println("ACCOUNT:" + option + "HAVE THE BALANCE OF: " + this.currentUser.getBalance(option));
        menu();
    }

    public void logout(){
        this.logged = false;
        this.currentUser = null;
        startMenu();
    }

    public void quit(){
        System.exit(1);
    }

}
