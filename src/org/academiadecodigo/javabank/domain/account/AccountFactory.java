package org.academiadecodigo.javabank.domain.account;

public class AccountFactory {

    public static Account getNewAccount(AccountType type, int number){
        Account account=null;
        switch (type){
            case SAVINGS:
                account = new SavingsAccount(number);
                break;
            case CHECKING:
                account = new CheckingAccount(number);
                break;
        }
        return account;
    }

}
